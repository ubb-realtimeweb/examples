#!/usr/bin/env ts-node

/**
 * Megkeresi az összes alfolderben levő node.js projektet
 * és meghívja a megadott parancsot a mappáiban
 */
import cp from 'child_process';
import fs from 'fs';
import path from 'path';
// eslint-disable-next-line import/extensions
import { hideBin } from 'yargs/helpers';
import yargs from 'yargs/yargs';

// rekurzív alprogram mely keresi a 'package.json'-t tartalmazó foldereket
function getProjectFolders(filename: string, baseFolder: string, noRoot: boolean): string[] {
  const ret = [];

  // ha a jelenlegi folderben van package.json, hozzáadjuk a listához
  if (!noRoot && fs.existsSync(path.join(baseFolder, filename))) {
    ret.push(baseFolder);
  }

  // lekérjük a folder tartalmát
  const contents = fs.readdirSync(baseFolder, { withFileTypes: true });
  // szűrjük hogy csak foldereket nézzünk, amelyek nem node_modules vagy .git
  contents
    .filter((file) => file.isDirectory() && !(['node_modules', '.next', '.git'].indexOf(file.name) > -1))
    .forEach((dir) => {
      // rekurzió
      ret.push(...getProjectFolders(filename, path.join(baseFolder, dir.name), false));
    });

  return ret;
}

function execute(projectFolder: string, cmd: string): Promise<void> {
  const args = cmd.split(' ');
  const proc = cp.spawn(args[0], args.slice(1), { env: process.env, cwd: projectFolder, stdio: 'inherit' });
  return new Promise((resolve, reject) => {
    proc?.stdout?.on('data', (x) => {
      process.stdout.write(x.toString());
    });
    proc?.stderr?.on('data', (x) => {
      process.stderr.write(x.toString());
    });
    proc?.on('exit', (code: number) => {
      if (code === 0) {
        resolve();
      } else {
        reject(Error(`Process exited with code ${code}`));
      }
    });
  });
}

async function executeInProjectFolders(filename: string, cmd: string, linear: boolean, noRoot: boolean) {
  const projectFolders = getProjectFolders(filename, process.cwd(), noRoot);
  console.log(`Found ${projectFolders.length} project folders`);
  console.log(projectFolders);

  const promises: Promise<void>[] = [];

  for (let i = 0; i < projectFolders.length; i++) {
    const projectFolder = projectFolders[i];
    console.log(`Calling "${cmd}" in project folder: ${projectFolder}`);

    if (linear) {
      // eslint-disable-next-line no-await-in-loop
      await execute(projectFolder, cmd);
    } else {
      promises.push(execute(projectFolder, cmd));
    }
  }

  await Promise.all(promises);
}

// parse cmd line arguments
const { filename, cmd, linear, noRoot } = yargs(hideBin(process.argv))
  .option('filename', { alias: 'f', type: 'string', default: 'package.json' })
  .option('cmd', { alias: 'c', type: 'string', default: 'npm i' })
  .option('linear', { alias: 'l', type: 'boolean', default: false })
  .option('noRoot', { alias: 'n', type: 'boolean', default: false })
  .parseSync();

console.log('File:', filename, 'Cmd:', cmd, 'Linear:', linear, 'Ignore root:', noRoot);
executeInProjectFolders(filename, cmd, linear, noRoot);
