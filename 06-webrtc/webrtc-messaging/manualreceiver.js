let receiveConnection; // fogadó fél kapcsolata
let receiveChannel;    // fogadó fél folyama

// válaszhoz szükséges üzenet
const answerMessage = {
  answer: undefined,
  icecandidates: [],
};

// bemenet tisztítása - XSS elkerülés
const sanitize = (string) => {
  const dummyElement = document.createElement('span');
  dummyElement.innerText = string;
  return dummyElement.innerHTML;
};

// új üzenet megjelenítése
const addMessage = (message) => {
  const elem = document.createElement('div');
  elem.innerHTML = `<b>Remote sender: </b>${sanitize(message.data)}`;
  document.getElementById('messages').appendChild(elem);
};

const onOffer = () => {
  // kiolvassuk a másik fél ajánlatát a szöveges bemenetből
  const offerMessage = JSON.parse(document.getElementById('offer').value);

  // fogadó kapcsolat megnyitása
  receiveConnection = new RTCPeerConnection();
  // lekezeljük, hogy mi történik ha adatfolyam érkezik
  receiveConnection.ondatachannel = (channelEvent) => {
    document.getElementById('status').innerText = 'Connected!';
    receiveChannel = channelEvent.channel;
    receiveChannel.onmessage = addMessage;
  };

  // közös ICE jelölteket biztosítunk
  receiveConnection.onicecandidate = (e) => {
    if (e.candidate) {
      answerMessage.icecandidates.push(e.candidate);
      document.getElementById('answer').value = JSON.stringify(answerMessage);
    }
  };

  // 1. a fogadó fél a beérkező címét beállítja a küldő fél kimenő címére
  receiveConnection.setRemoteDescription(offerMessage.offer)
    // 2. a fogadó fél választ generál (itt kap ő ICE nevet)
    .then(() => receiveConnection.createAnswer())
    // 3. a fogadó fél beállítja saját leírását
    .then((answer) => {
      answerMessage.answer = answer;
      receiveConnection.setLocalDescription(answer);
    })
    // 4. a fogadó fél publikálja a válaszát
    .then(() => {
      // ICE candidate-ek importálása
      offerMessage.icecandidates.forEach((c) => receiveConnection.addIceCandidate(c));

      document.getElementById('status').innerText = 'Offer received, answer generated';
      document.getElementById('answer').value = JSON.stringify(answerMessage);
    })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

window.onload = () => {
  document.getElementById('offerBtn').onclick = onOffer;
};
