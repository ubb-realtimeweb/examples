let sendConnection;    // küldő fél kapcsolata
let sendChannel;       // küldő fél folyama
let receiveConnection; // fogadó fél kapcsolata
let receiveChannel;    // fogadó fél folyama

// bemenet tisztítása - XSS elkerülés
const sanitize = (string) => {
  const dummyElement = document.createElement('span');
  dummyElement.innerText = string;
  return dummyElement.innerHTML;
};

// új üzenet megjelenítése
const addMessage = (message) => {
  const elem = document.createElement('div');
  elem.innerHTML = `<b>Echo: </b>${sanitize(message.data)}`;
  document.getElementById('messages').appendChild(elem);
};

const connectPeers = () => {
  // küldő kapcsolat megnyitása
  sendConnection = new RTCPeerConnection();
  // adatfolyam megnyitása
  sendChannel = sendConnection.createDataChannel('sendChannel');

  // fogadó kapcsolat megnyitása
  receiveConnection = new RTCPeerConnection();
  // lekezeljük, hogy mi történik ha adatfolyam érkezik
  receiveConnection.ondatachannel = (channelEvent) => {
    receiveChannel = channelEvent.channel;
    receiveChannel.onmessage = addMessage;
  };

  // közös ICE jelölteket biztosítunk
  sendConnection.onicecandidate = (e) => {
    if (e.candidate) {
      receiveConnection.addIceCandidate(e.candidate);
    }
  };
  receiveConnection.onicecandidate = (e) => {
    if (e.candidate) {
      sendConnection.addIceCandidate(e.candidate);
    }
  };

  // P2P kapcsolat létesítése
  // 1. a küldő fél ajánlatot generál (itt kap ICE nevet)
  sendConnection.createOffer()
    // 2. a küldő fél beállítja saját leírását
    .then((offer) => sendConnection.setLocalDescription(offer))
    // 3. a fogadó fél a beérkező címét beállítja a küldő fél kimenő címére
    .then(() => receiveConnection.setRemoteDescription(sendConnection.localDescription))
    // 4. a fogadó fél választ generál (itt kap ő ICE nevet)
    .then(() => receiveConnection.createAnswer())
    // 5. a fogadó fél beállítja saját leírását
    .then((answer) => receiveConnection.setLocalDescription(answer))
    // 6. a küldő fél a beérkező címét beállítja a fogadó fél kimenő címére
    .then(() => sendConnection.setRemoteDescription(receiveConnection.localDescription))
    // siker
    .then(() => { document.getElementById('status').innerText = 'Connected!'; })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

// gombnyomás az üzenetnél
const sendMessage = (keyPressEvent) => {
  // ENTER esetén
  if (keyPressEvent.keyCode === 13) {
    // üzenetet küldünk
    const message = document.getElementById('messageInput').value;
    sendChannel.send(message);
    // a szöveges bemenetet ürítjük
    document.getElementById('messageInput').value = '';
  }
};

window.onload = () => {
  connectPeers();
  // gombnyomás az üzenetnél
  document.getElementById('messageInput').onkeypress = sendMessage;
};
