let sendConnection;    // küldő fél kapcsolata
let sendChannel;       // küldő fél folyama

// STOMP kliens kapcsolati objektum deklarálása
let client;
let connectionId;

const connectPeers = () => {
  // küldő kapcsolat megnyitása
  sendConnection = new RTCPeerConnection();
  // adatfolyam megnyitása
  sendChannel = sendConnection.createDataChannel('sendChannel');

  // közös ICE jelölteket biztosítunk
  sendConnection.onicecandidate = (e) => {
    if (e.candidate) {
      const headers = { 'Content-Type': 'application/json', 'auto-delete': true };
      client.send(`/queue/webrtc-messaging-sender-icecandidate-${connectionId}`, headers, JSON.stringify(e.candidate));
    }
  };

  // P2P kapcsolat létesítése
  // 1. a küldő fél ajánlatot generál (itt kap ICE nevet)
  sendConnection.createOffer()
    // 2. a küldő fél beállítja saját leírását
    .then((offer) => sendConnection.setLocalDescription(offer))
    // 3. beírjuk az ajánlatot a text fieldbe, a fogadófélnek ismernie kell
    .then(() => {
      document.getElementById('status').innerText = 'Offer generated, waiting for answer';
      // elküldjük az offert az offer topicra
      const message = {
        offer: sendConnection.localDescription,
        connectionId,
      };
      client.send('/queue/webrtc-messaging-offer', { 'Content-Type': 'application/json' }, JSON.stringify(message));
    })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

const onIcecandidate = (message) => {
  const icecandidate = JSON.parse(message.body);
  sendConnection.addIceCandidate(icecandidate);
};

const onAnswer = (message) => {
  const answer = JSON.parse(message.body);

  // a küldő fél a beérkező címét beállítja a fogadó fél kimenő címére
  sendConnection.setRemoteDescription(answer)
    // siker
    .then(() => {
      document.getElementById('status').innerHTML = 'Connected!';
    })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

// gombnyomás az üzenetnél
const sendMessage = (keyPressEvent) => {
  // ENTER esetén
  if (keyPressEvent.keyCode === 13) {
    // üzenetet küldünk
    const message = document.getElementById('messageInput').value;
    sendChannel.send(message);
    // a szöveges bemenetet ürítjük
    document.getElementById('messageInput').value = '';
  }
};

window.onload = () => {
  // egyedi azonosító generálása
  connectionId = btoa(crypto.getRandomValues(new Uint8Array(2)));

  // Kapcsolat megnyitása
  // RabbitMQ STOMP-over-WebSocket kapcsolata
  // port = 15674, subprotocol = STOMP
  /* eslint-disable no-restricted-globals */
  /* eslint-disable no-undef */
  client = Stomp.client(`ws://${location.hostname}:15674/ws`);

  // sikeres kapcsolat esetén
  const onConnect = () => {
    connectPeers();
    // figyelünk a dedikált answer nevű queue-ra
    client.subscribe(`/queue/webrtc-messaging-answer-${connectionId}`, onAnswer, { 'auto-delete': true });
    // figyelünk a dedikált icecandidate-ekre
    client.subscribe(`/queue/webrtc-messaging-receiver-icecandidate-${connectionId}`, onIcecandidate,  { 'auto-delete': true });
  };

  // STOMP kapcsolat
  client.connect('guest', 'guest', onConnect, console.error, '/');

  document.getElementById('messageInput').onkeypress = sendMessage;
};
