let sendConnection;    // küldő fél kapcsolata
let receiveConnection; // fogadó fél kapcsolata

let stream;

const connectPeers = () => {
  // küldő kapcsolat megnyitása
  sendConnection = new RTCPeerConnection();

  // hozzákötjük a trackekhez az RTC kapcsolatot
  stream.getTracks().forEach((track) => {
    sendConnection.addTrack(track, stream);
  });

  // fogadó kapcsolat megnyitása
  receiveConnection = new RTCPeerConnection();
  // lekezeljük, hogy mi történik ha adatfolyam érkezik
  receiveConnection.ontrack = (track) => {
    document.getElementById('status').innerText = 'Connected!';
    const remoteVideo = document.getElementById('remote-video');
    [remoteVideo.srcObject] = track.streams;
  };

  // közös ICE jelölteket biztosítunk
  sendConnection.onicecandidate = (e) => {
    if (e.candidate) {
      receiveConnection.addIceCandidate(e.candidate);
    }
  };
  receiveConnection.onicecandidate = (e) => {
    if (e.candidate) {
      sendConnection.addIceCandidate(e.candidate);
    }
  };

  // P2P kapcsolat létesítése
  // 1. a küldő fél ajánlatot generál (itt kap ICE nevet)
  sendConnection.createOffer()
    // 2. a küldő fél beállítja saját leírását
    .then((offer) => sendConnection.setLocalDescription(offer))
    // 3. a fogadó fél a beérkező címét beállítja a küldő fél kimenő címére
    .then(() => receiveConnection.setRemoteDescription(sendConnection.localDescription))
    // 4. a fogadó fél választ generál (itt kap ő ICE nevet)
    .then(() => receiveConnection.createAnswer())
    // 5. a fogadó fél beállítja saját leírását
    .then((answer) => receiveConnection.setLocalDescription(answer))
    // 6. a küldő fél a beérkező címét beállítja a fogadó fél kimenő címére
    .then(() => sendConnection.setRemoteDescription(receiveConnection.localDescription))
    // siker
    .then(() => { document.getElementById('status').innerText = 'Connected!'; })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

window.onload = () => {
  navigator.mediaDevices.getUserMedia({ audio: false, video: true })
    .then((newStream) => {
      stream = newStream;
      // a lokális stream bemeneteként állítjuk
      const localVideo = document.getElementById('local-video');
      localVideo.srcObject = stream;

      connectPeers();
    })
    .catch((err) => { console.error(err); });
};
