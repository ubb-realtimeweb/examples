import fs from 'fs';
import http from 'http';
import path from 'path';
import { Server } from 'socket.io';

//
// socket io DTOs
//

interface ServerToClientEvents {
  addFilename: (filename: string) => void;
}

interface ClientToServerEvents {
  chunk: (chunk: Buffer) => void;
}

const uploadDir = path.join(process.cwd(), 'uploads');

// meglévő állományok karbantartása
let filenames: string[];
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
  filenames = [];
} else {
  filenames = fs.readdirSync(uploadDir);
}

const server = http.createServer();

// Socket.IO szerver
const io = new Server<ClientToServerEvents, ServerToClientEvents>(server, {
  cors: { origin: '*' },
});

// websocket csatlakozásának lekezelése
io.on('connection', (socket) => {
  // allokálunk egy állománynevet, ahova streameljük az ide érkezetteket
  // nyitunk felé is egy streamet
  const filename = `upload-${new Date().toISOString().replace(/[-:.TZ]/g, '')}.webm`;
  const writeStream = fs.createWriteStream(path.join(uploadDir, filename));

  filenames.push(filename);
  console.log(`[${filename}] connected`);

  socket.on('chunk', (chunk: Buffer) => {
    console.log(`[${filename}] new partial upload chunk of size ${chunk.length}`);
    writeStream.write(chunk);
  });

  // ha befejeződött a streamelés, zárjuk az állományt
  socket.on('disconnect', () => {
    console.log(`[${filename}] disconnected`);
    if (writeStream?.bytesWritten > 0) {
      writeStream.end();
    }
  });

  // már meglévő állományokat visszaküldjük a kliensnek
  filenames.forEach((fn) => socket.emit('addFilename', fn));
});

const port = process.env.PORT || 8080;
server.listen(port, () => {
  console.log(`Server listening on http://localhost:${port}/ ...`);
});
