import { createRoot } from 'react-dom/client';

import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import './style.css';

function Html5PictureElement() {
  return (
    <>
      <h1>
        Képek (<code>&lt;picture&gt;</code> elem)
      </h1>

      <div>
        <picture className="media-container">
          <source media="(max-width: 300px)" srcSet="/floods-160.jpg" type="image/jpeg" />
          <source media="(max-width: 460px)" srcSet="/floods-320.jpg" type="image/jpeg" />
          <source media="(max-width: 620px)" srcSet="/floods-480.jpg" type="image/jpeg" />
          <source srcSet="/floods-640.jpg" type="image/jpeg" />
          {/* Az alábbi a default kép, ha a böngésző nem találja megfelelőnek a fentieket
          vagy hogyha a nem támogatja a picture elemet  */}
          <img src="/floods-480.jpg" />
        </picture>
      </div>
    </>
  );
}

function Html5AudioElement() {
  return (
    <>
      <h1>
        Hang (<code>&lt;audio&gt;</code> elem)
      </h1>

      <div>
        <audio className="media-container" controls>
          <source src="/a2002011001-e02-128k.ogg" type="audio/ogg" />
          <source src="/a2002011001-e02-128k.mp3" type="audio/mpeg" />
          <a href="/a2002011001-e02-128k.ogg" target="_blank">
            A böngészője nem támogatja a HTML5 media elemeket. Letöltés
          </a>
        </audio>
      </div>
    </>
  );
}

function Html5VideoElement() {
  return (
    <>
      <h1>
        Video (<code>&lt;video&gt;</code> elem)
      </h1>

      <div>
        <video className="media-container" controls>
          <source src="/flower.webm" type="video/webm" />
          <source src="/flower.mp4" type="video/mp4" />
          <a href="/flower.mp4" target="_blank">
            A böngészője nem támogatja a HTML5 media elemeket. Letöltés
          </a>
        </video>
      </div>
    </>
  );
}

function Html5VideoWithSubtitles() {
  return (
    <>
      <h1>
        Feliratok (<code>&lt;track&gt;</code> elem)
      </h1>

      <div>
        <video className="media-container" src="/friday.mp4" controls>
          <track srcLang="en" label="English" src="/friday.vtt" default />
          <track srcLang="hu" label="Magyar" src="/friday-hu.vtt" />
          <a href="/friday.mp4" target="_blank">
            A böngészője nem támogatja a HTML5 media elemeket. Letöltés
          </a>
        </video>
      </div>
    </>
  );
}

function Html5MediaElements() {
  return (
    <BrowserRouter>
      <nav>
        <Link to="/picture">Picture</Link>
        <Link to="/audio">Audio</Link>
        <Link to="/video">Video</Link>
        <Link to="/video-with-subtitles">Video w/ subtitles</Link>
      </nav>
      <Routes>
        <Route path="/picture" element={<Html5PictureElement />} />
        <Route path="/audio" element={<Html5AudioElement />} />
        <Route path="/video" element={<Html5VideoElement />} />
        <Route path="/video-with-subtitles" element={<Html5VideoWithSubtitles />} />
      </Routes>
    </BrowserRouter>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<Html5MediaElements />);
