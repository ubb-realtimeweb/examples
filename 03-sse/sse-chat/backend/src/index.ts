/**
 * Chat alkalmazás SSE segítségével.
 *
 * POST /chat - fetch endpoint üzenetek felküldésére
 * GET /chat - SSE endpoint üzenetek broadcastingjére (relay)
 */
import cors from 'cors';
import express, { Request, Response } from 'express';

//
// esemény DTO
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

//
// tároljuk a klienseket - ide helyezzük az
// express response objektumokat, hogy streameljünk rájuk
//

const clients: Record<string, Response> = {};

const app = express();
app.use(cors());

/**
 * GET üzenet a /chatre = feliratkozás az SSE broadcastre
 */
app.get('/chat', (req, res) => {
  // clientId érkezik query paramként
  const clientId = `${req.query.clientId}`;

  // SSE-hez szükséges headerek
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    Connection: 'keep-alive',
    'Cache-Control': 'no-cache',
  });

  // tároljuk a félkész választ, mint aktív kliens
  clients[clientId] = res;

  // ha a kliens zárja a kapcsolatot, töröljük a listából
  req.on('close', () => {
    console.log(`[${clientId}] connection closed`);
    delete clients[clientId];
  });
});

/**
 * POST üzenet a /chatre = új üzenet, amit kiküldünk
 * minden más kliensnek
 */
app.post('/chat', express.json(), (req: Request<unknown, unknown, BroadcastMessage>, res: Response) => {
  const message = JSON.stringify(req.body);
  console.log(`New message: ${message}, relaying`);

  // minden kliensnek küldjük az üzenetet
  Object.entries(clients).forEach(([clientId, response]) => {
    console.log(`[${clientId}] sending '${message}'`);
    // az üres sor a végén kiküldeti az üzenetet
    response.write(`data: ${message}\n\n`);
  });

  // egyből visszatérítjük az OK-t
  res.send();
});

const port = process.env.WEB_PORT || 8080;
app.listen(port, () => {
  console.log(`Server listening on http://localhost:${port}/ ...`);
});
