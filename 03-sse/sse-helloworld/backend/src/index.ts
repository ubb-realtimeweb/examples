/**
 * Chat alkalmazás SSE segítségével.
 *
 * GET /messages - SSE messages
 */
import cors from 'cors';
import express from 'express';

//
// esemény DTO
//

type Message = {
  text: string;
  time: string;
};

const app = express();
app.use(cors());

/**
 * GET üzenet a /chatre = feliratkozás az SSE broadcastre
 */
app.get('/messages', (req, res) => {
  console.log('Connection opened');

  // SSE-hez szükséges headerek
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    Connection: 'keep-alive',
    'Cache-Control': 'no-cache',
  });

  // 5 másodpercenként küldünk egy üzenetet a kliensnek
  const intervalHandler = setInterval(() => {
    // felépítjük az üzenetet
    const message: Message = {
      text: 'Hello from server',
      time: new Date().toISOString(),
    };

    // az üres sor a végén kiküldeti az üzenetet
    res.write(`data: ${JSON.stringify(message)}\n\n`);
  }, 5000);

  // ha a kliens zárja a kapcsolatot, lezárjuk az időzítőt
  req.on('close', () => {
    console.log('Connection closed');
    clearInterval(intervalHandler);
  });
});

const port = process.env.WEB_PORT || 8080;
app.listen(port, () => {
  console.log(`Server listening on http://localhost:${port}/ ...`);
});
