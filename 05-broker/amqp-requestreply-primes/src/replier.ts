import * as amqp from 'amqplib';

//
// interakció DTO
//

type PrimeRequest = {
  number: number;
  replyQueue: string;
};

type PrimeReply = {
  number: number;
  result: boolean;
};

const requestQueue = 'isprime';

/**
 * Ellenőrzés, hogy n prim-e
 */
function isPrime(n: number) {
  if (!Number.isInteger(n) || n === 1) {
    return false;
  }
  if (n === 2) {
    return true;
  }
  for (let x = 3; x < n; x += 2) {
    if (n % x === 0) {
      return false;
    }
  }
  return true;
}

(async () => {
  const connection = await amqp.connect({});
  const channel = await connection.createChannel();
  await channel.prefetch(1);

  await channel.assertQueue(requestQueue);

  channel.consume(requestQueue, (message) => {
    if (message) {
      const content = JSON.parse(message.content.toString()) as PrimeRequest;
      console.log('Received following work order', content);

      const response: PrimeReply = {
        number: content.number,
        result: isPrime(content.number),
      };

      console.log('Sending following response', response);
      // válasz-queue a kérésben
      channel.sendToQueue(content.replyQueue, Buffer.from(JSON.stringify(response)));
      channel.ack(message);
    }
  });

  // SIGINT signal (Ctrl+C) esetén zárjuk a kapcsolatot
  process.once('SIGINT', async () => {
    console.log('Closing AMQP connection');
    await channel.close();
    await connection.close();
  });

  console.log('Listening to prime work orders');
})();
