# AMQP Spring example

- AMQP message broker (tested with RabbitMQ)
  - version `3.8+` running on `localhost:5672`
  - both username and password are `guest`

## Testing

- Running with Spring profile "listener" listens on queue "hello" and prints messages
    - `SPRING_PROFILES_ACTIVE=listener gradle bootRun`
- Running with Spring profile "sender" sends messages to "hello" every 15 seconds
    - `SPRING_PROFILES_ACTIVE=sender gradle bootRun`