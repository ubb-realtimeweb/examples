package edu.bbte.realtimeweb.amqpexample.errorhandling;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

/**
 * RabbitMQ hibakezelő:
 * Hogyan reagáljon arra, ha kezeletlen kivétel történik egy üzenet feldolgozásakor
 */
@Component
@Slf4j
public class MessageQueueErrorHandler implements ErrorHandler {

    @Override
    public void handleError(Throwable exception) {
        log.error("Message processing failed", exception);
        // nem tesszük vissza az üzenetet a queue-ba, így nem lesz végtelen ciklus
        throw new AmqpRejectAndDontRequeueException("Message processing failed", exception);
    }
}
