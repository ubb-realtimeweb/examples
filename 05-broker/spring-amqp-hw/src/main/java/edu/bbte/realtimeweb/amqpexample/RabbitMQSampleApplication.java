package edu.bbte.realtimeweb.amqpexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RabbitMQSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMQSampleApplication.class, args);
    }
}
