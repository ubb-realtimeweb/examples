/* eslint-disable no-undef */
/* eslint-disable no-restricted-globals */

// STOMP kliens kapcsolati objektum deklarálása
let client;
let subscription;

// új üzenet megjelenítése
const addMessage = (message) => {
  const body = JSON.parse(message.body);

  const elem = document.createElement('div');
  elem.setAttribute('class', body.logLevel);

  elem.innerHTML = `${body.clientId} sent:
    ${body.time} ${body.logLevel} - ${body.text}`;

  document.getElementById('messages').appendChild(elem);
};

// iratkozás a topic-ra megfelelő routing patternnel
const subscribeToLogs = () => {
  // lekérjük a legördülő lista értékét, lehet "*" is
  const logLevel = document.getElementById('logLevelFilterInput').value;

  // ha már fel voltunk iratkozva a topic-ra, előbb szükséges azt lemondanunk
  if (subscription) {
    client.unsubscribe(subscription.id);
  }

  // az iratkozásban a topic után megadjuk a kulcsot, ez tartalmazhat wildcardokat is
  subscription = client.subscribe(`/topic/logger.${logLevel}`, addMessage);
};

window.onload = () => {
  // Kapcsolat megnyitása
  // RabbitMQ STOMP-over-WebSocket kapcsolata
  // port = 15674, subprotocol = STOMP
  client = Stomp.client(`ws://${location.hostname}:15674/ws`);

  // sikeres kapcsolat esetén
  const onConnect = () => {
    document.getElementById('status').innerHTML = '<b>Connected</b>';
    subscribeToLogs();
  };

  // STOMP kapcsolat
  client.connect('guest', 'guest', onConnect, console.error, '/');

  // amikor megváltozik a log szint a legördülő listában,
  // újrairatkozunk a megfelelő routing patternnel.
  document.getElementById('logLevelFilterInput').onchange = subscribeToLogs;
};
