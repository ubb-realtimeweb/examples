/* eslint-disable no-undef */
/* eslint-disable no-restricted-globals */

// STOMP kliens kapcsolati objektum deklarálása
let client;
let clientId;

// gombnyomás az üzenetnél
const sendMessage = (keyPressEvent) => {
  // ENTER esetén
  if (keyPressEvent.keyCode === 13) {
    const logLevel = document.getElementById('logLevelInput').value;
    // üzenetet küldünk
    const message = {
      clientId,
      logLevel,
      time: new Date(),
      text: document.getElementById('messageInput').value,
    };

    // Az alábbi send az alapértelmezett amq.topic-ra megy
    // s a routing key-je a logLevel szerint változik
    // custom exchange esetén ez lehet /exchange/mytopic/logger...
    client.send(`/topic/logger.${logLevel}`, { 'Content-Type': 'application/json' }, JSON.stringify(message));
    // a szöveges bemenetet ürítjük
    document.getElementById('messageInput').value = '';
  }
};

window.onload = () => {
  // egyedi azonosító generálása
  clientId = btoa(crypto.getRandomValues(new Uint8Array(2)));

  // Kapcsolat megnyitása
  // RabbitMQ STOMP-over-WebSocket kapcsolata
  // port = 15674, subprotocol = STOMP
  client = Stomp.client(`ws://${location.hostname}:15674/ws`);

  // sikeres kapcsolat esetén
  const onConnect = () => {
    document.getElementById('status').innerHTML = `<b>Connected as ${clientId}!</b>`;
  };

  // STOMP kapcsolat
  client.connect('guest', 'guest', onConnect, console.error, '/');

  // gombnyomás az üzenetnél
  document.getElementById('messageInput').onkeypress = sendMessage;
};
