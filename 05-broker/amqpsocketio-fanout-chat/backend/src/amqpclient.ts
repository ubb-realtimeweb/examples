import * as amqp from 'amqplib';
import { randomBytes } from 'crypto';

export type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

const exchange = 'chat';
let channel: amqp.Channel;

/**
 * Kapcsolat megnyítása
 */
export async function connectToRabbitMq(): Promise<amqp.Channel> {
  console.log('Connecting to AMQP...');
  const connection = await amqp.connect({});
  channel = await connection.createChannel();

  process.once('SIGINT', async () => {
    // zárjuk a csatornát és kapcsolatot
    console.log('\nClosing AMQP connection');
    await channel.close();
    await connection.close();
  });

  // erőforrások létezése
  await channel.assertExchange(exchange, 'fanout');
  return channel;
}

/**
 * Az egy kliensnek szükséges broker kapcsolat
 */
export class RabbitMqChatClient {
  clientId: string;

  queueName: string;

  constructor() {
    this.clientId = randomBytes(8).toString('base64');
    this.queueName = `chat-${this.clientId}`;
  }

  /**
   * Kapcsolat kiegészítése az egy kliensnek szükséges queue és bindinggal
   */
  async createClientQueue() {
    await channel.assertQueue(this.queueName, { exclusive: true });
    await channel.bindQueue(this.queueName, exchange, '');
  }

  /**
   * Üzenet küldése
   */
  sendMessage(text: string): boolean {
    const message: BroadcastMessage = {
      clientId: this.clientId,
      text,
      time: new Date().toISOString(),
    };
    // a routing key üres, mivel a fanout nem veszi figyelembe
    return channel.publish(exchange, '', Buffer.from(JSON.stringify(message)));
  }

  /**
   * Üzenet fogadása
   */
  onMessage(callback: (message: BroadcastMessage) => void) {
    // a dedikált queue-ra figyelünk
    channel.consume(this.queueName, (data) => {
      if (data) {
        const message = JSON.parse(data.content.toString()) as BroadcastMessage;
        callback(message);
        channel.ack(data);
      }
    });
  }
}
