import * as amqp from 'amqplib';

/**
 * Egyszerű üzenetekre való figyelés AMQP protokollal
 * a lokálisan futó RabbitMQ "hello" nevű queue-jából.
 * Ebben a példában ez a szkript játsza a "consumer" szerepet.
 */
(async () => {
  // csatlakozás, default URL: amqp://guest:guest@localhost:5672/
  console.log('Establishing AMQP connection');
  const connection = await amqp.connect(
    {},
    {
      clientProperties: {
        connection_name: 'amqp-receiver',
      },
    },
  );

  // kommunikációs csatorna kialakítása
  const channel = await connection.createChannel();

  // kérjük a "hello" nevű queue létrehozását, ha még nem létezik
  // durable=false - a bróker újraindításával a queue törlődik
  console.log('Asserting queue "hello"');
  await channel.assertQueue('hello', { durable: false });

  // figyelünk a "hello" nevű queue-ra küldött üzenetekre
  // minden beérkezett üzenet a callback függvényt triggereli
  channel.consume('hello', (message) => {
    if (message) {
      // loggoljuk az üzenetet
      console.log('Received following message:', message?.content?.toString());
      // jóváhagyjuk az üzenetet
      channel.ack(message);
    }
  });

  // SIGINT signal (Ctrl+C) esetén zárjuk a kapcsolatot
  process.once('SIGINT', async () => {
    console.log('Closing AMQP connection');
    await channel.close();
    await connection.close();
  });

  console.log('Listening for messages');
})();
