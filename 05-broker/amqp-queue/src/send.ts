import * as amqp from 'amqplib';
import { createInterface } from 'readline';

/**
 * Egy egyszerű üzenet küldése AMQP protokollal a lokálisan futó RabbitMQ "hello" nevű queue-jába.
 * Ebben a példában ez a szkript játsza a "producer" szerepet.
 */
(async () => {
  // csatlakozás, default URL: amqp://guest:guest@localhost:5672/
  console.log('Establishing AMQP connection');
  const connection = await amqp.connect(
    {},
    {
      clientProperties: {
        connection_name: 'amqp-sender',
      },
    },
  );
  // kommunikációs csatorna kialakítása
  const channel = await connection.createChannel();

  // kérjük a "hello" nevű queue létrehozását, ha még nem létezik
  // durable=false - a bróker újraindításával a queue törlődik
  console.log('Asserting queue "hello"');
  await channel.assertQueue('hello', { durable: false });

  // beolvasunk a standard bemenetről sorokat, melyeket kiküldünk
  const rl = createInterface(process.stdin);
  console.log('Enter text to send to queue');

  rl.on('line', (message) => {
    // elküldjük az üzenetet puffer formájában a "hello" queue-ba
    console.log('Sending following message:', message);
    channel.sendToQueue('hello', Buffer.from(message));
  });

  rl.on('close', async () => {
    // zárjuk a csatornát és kapcsolatot
    rl.close();
    console.log('Closing AMQP connection');
    await channel.close();
    await connection.close();
  });
})();
