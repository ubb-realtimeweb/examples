import * as amqp from 'amqplib';
import { createInterface } from 'readline';

(async () => {
  const connection = await amqp.connect({});
  const channel = await connection.createChannel();

  // exchange készítése
  await channel.assertExchange('example-direct-exchange', 'direct');
  // sor készítése
  await channel.assertQueue('example-queue');
  // az exchange és sor összekötése
  await channel.bindQueue('example-queue', 'example-direct-exchange', 'example-queue');

  // beolvasunk a standard bemenetről sorokat, melyeket kiküldünk
  const rl = createInterface(process.stdin);
  console.log('Enter text to send to queue');

  rl.on('line', (message) => {
    // elküldjük az üzenetet puffer formájában a "hello" queue-ba
    console.log('Sending message to exchange:', message);
    channel.publish('example-direct-exchange', 'example-queue', Buffer.from(message));
  });

  rl.on('close', async () => {
    // zárjuk a csatornát és kapcsolatot
    rl.close();
    console.log('Closing AMQP connection');
    await channel.close();
    await connection.close();
  });
})();
