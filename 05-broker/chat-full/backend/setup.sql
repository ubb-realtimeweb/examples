-- mysql -u root -p <setup.sql
CREATE DATABASE IF NOT EXISTS chat;
USE chat;
GRANT ALL PRIVILEGES ON *.* TO 'chat'@'%' IDENTIFIED BY 'chat';
FLUSH PRIVILEGES;
