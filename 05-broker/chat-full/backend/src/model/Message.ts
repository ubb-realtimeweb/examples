/* eslint-disable import/no-cycle */
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Channel from './Channel';
import User from './User';

@Entity()
export default class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  text: string;

  @Column()
  time: Date;

  @ManyToOne(() => User, { eager: true })
  user: User | Partial<User>;

  @ManyToOne(() => Channel, { eager: true })
  channel: Channel | Partial<Channel>;
}
