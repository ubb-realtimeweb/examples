import { MessageCreationDto, MessageDto, WsMessageCreationDto } from '../dto/MessageDto';
import Message from '../model/Message';

export function wsDtoToModel(dto: WsMessageCreationDto): Partial<Message> {
  return {
    text: dto.text,
    time: new Date(),
    channel: {
      id: dto.channelId,
    },
    user: {
      id: dto.userId,
    },
  };
}

export function dtoToModel(channelId: number, dto: MessageCreationDto): Partial<Message> {
  return {
    text: dto.text,
    time: new Date(),
    channel: {
      id: channelId,
    },
    user: {
      id: dto.userId,
    },
  };
}

export function modelToDto(message: Message): MessageDto {
  return {
    id: message.id,
    text: message.text,
    time: message.time.toISOString(),
    user: {
      id: message.user.id,
      name: message.user.name,
    },
  };
}

export function modelsToDtos(messages: Message[]): MessageDto[] {
  return messages.map(this.modelToDto);
}
