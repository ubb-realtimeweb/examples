import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import User from '../model/User';

@Injectable()
export default class UserService {
  @Inject('UserRepository')
  private readonly userRepository: Repository<User>;

  findAllUsers(): Promise<User[]> {
    return this.userRepository.find();
  }

  findUsersByName(name: string): Promise<User[]> {
    return this.userRepository.findBy({ name });
  }

  createUser(user: Partial<User>): Promise<User> {
    return this.userRepository.save(user, { reload: true });
  }
}
