import { Inject, Injectable } from '@nestjs/common';
import { LessThan, Repository } from 'typeorm';
import Message from '../model/Message';

@Injectable()
export default class MessageService {
  @Inject('MessageRepository')
  private readonly messageRepository: Repository<Message>;

  async findMessages(channelId: number, from: number, num: number): Promise<Message[]> {
    const messages = await this.messageRepository.find({
      where: { channel: { id: channelId }, id: LessThan(from) },
      take: num,
      order: { time: 'desc' },
    });
    return messages.reverse();
  }

  async createMessage(message: Partial<Message>): Promise<Message> {
    const result = await this.messageRepository.insert(message);
    return this.messageRepository.findOneBy({ id: result.identifiers[0].id });
  }
}
