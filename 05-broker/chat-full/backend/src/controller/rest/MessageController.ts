import { Body, Controller, Get, Inject, Param, Post, Query } from '@nestjs/common';
import { MessageCreationDto, MessageDto } from '../../dto/MessageDto';
import * as messageMapper from '../../mapper/MessageMapper';
import MessageService from '../../service/MessageService';

@Controller('/backend/channels/:channelId/messages')
export default class MessageController {
  @Inject()
  private messageService: MessageService;

  @Get()
  async findMessages(
    @Param('channelId') channelId: string,
    @Query('from') from: string,
    @Query('num') num: string,
  ): Promise<MessageDto[]> {
    const messages = await this.messageService.findMessages(
      Number(channelId),
      Number(from || Number.MAX_SAFE_INTEGER),
      Number(num || 10),
    );
    return messageMapper.modelsToDtos(messages);
  }

  @Post()
  async createMessage(@Param('channelId') channelId: string, @Body() dto: MessageCreationDto): Promise<MessageDto> {
    const model = messageMapper.dtoToModel(Number(channelId), dto);
    const message = await this.messageService.createMessage(model);
    return messageMapper.modelToDto(message);
  }
}
