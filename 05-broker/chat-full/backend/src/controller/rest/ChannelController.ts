import { Body, Controller, Delete, Get, HttpCode, Inject, NotFoundException, Param, Post } from '@nestjs/common';
import { ChannelCreationDto, ChannelDto } from '../../dto/ChannelDto';
import * as channelMapper from '../../mapper/ChannelMapper';
import ChannelService from '../../service/ChannelService';

@Controller('/backend/channels')
export default class ChannelController {
  @Inject()
  private channelService: ChannelService;

  @Get()
  async findAllChannels(): Promise<ChannelDto[]> {
    const channels = await this.channelService.findAllChannels();
    return channelMapper.modelsToDtos(channels);
  }

  @Get('/:channelId')
  async findChannelById(@Param('channelId') channelId: number): Promise<ChannelDto> {
    const channel = await this.channelService.findChannelById(channelId);
    if (!channel) {
      throw new NotFoundException();
    }
    return channelMapper.modelToDto(channel);
  }

  @Post()
  async createChannel(@Body() dto: ChannelCreationDto): Promise<ChannelDto> {
    const model = channelMapper.dtoToModel(dto);
    const channel = await this.channelService.createChannel(model);
    return channelMapper.modelToDto(channel);
  }

  @Delete('/:channelId')
  @HttpCode(204)
  async deleteChannelById(@Param('channelId') channelId: number): Promise<void> {
    const success = await this.channelService.deleteChannelById(channelId);
    if (!success) {
      throw new NotFoundException();
    }
  }
}
