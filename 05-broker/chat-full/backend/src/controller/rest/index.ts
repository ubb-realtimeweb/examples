import ChannelController from './ChannelController';
import MessageController from './MessageController';
import UserController from './UserController';

const restControllers = [ChannelController, MessageController, UserController];
export default restControllers;
