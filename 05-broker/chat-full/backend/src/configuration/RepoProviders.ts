import { DataSource } from 'typeorm';
import models from '../model';

const repoProviders = models.map((T) => ({
  provide: `${T.name}Repository`,
  useFactory: (dataSource: DataSource) => dataSource.getRepository(T),
  inject: [DataSource],
}));

export default repoProviders;
