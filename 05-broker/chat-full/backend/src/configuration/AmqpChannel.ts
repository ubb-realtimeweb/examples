import { connect, Channel } from 'amqplib';

export async function initializeAmqpChannel(): Promise<Channel> {
  const connection = await connect({
    hostname: process.env.RABBITMQ_HOST || 'localhost',
    port: Number(process.env.RABBITMQ_PORT) || 5672,
    username: process.env.RABBITMQ_USER || 'guest',
    password: process.env.RABBITMQ_PASSWORD || 'guest',
  });

  const channel = await connection.createChannel();
  await channel.assertExchange('chat', 'topic', { durable: true });
  return channel;
}

const AmqpChannelProvider = {
  provide: 'AMQP_CHANNEL',
  useFactory: () => initializeAmqpChannel(),
};
export default AmqpChannelProvider;
