import { UserDto } from './UserDto';

export type MessageDto = {
  id: number;
  text: string;
  time: string;
  user: UserDto;
};

export type MessageCreationDto = {
  userId: number;
  text: string;
};

export type WsMessageCreationDto = {
  channelId: number;
  userId: number;
  text: string;
};
