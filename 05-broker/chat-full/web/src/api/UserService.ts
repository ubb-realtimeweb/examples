import { baseApiUrl } from '../constants';
import { UserCreationDto, UserDto } from '../dto/UserDto';

export const fetchUsers = async (name?: string): Promise<UserDto[]> => {
  const params = new URLSearchParams({ name }).toString();
  const response = await fetch(`${baseApiUrl}/users?${params}`);
  const entities = (await response.json()) as UserDto[];
  return entities;
};

export const createUser = async (user: UserCreationDto): Promise<UserDto> => {
  const response = await fetch(`${baseApiUrl}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(user),
  });

  const entity = (await response.json()) as UserDto;
  return entity;
};

export const fetchOrCreateUser = async (name: string): Promise<UserDto> => {
  const users = await fetchUsers(name);
  if (!users.length) {
    return createUser({ name });
  }
  return users[0];
};
