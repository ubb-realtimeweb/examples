import { baseApiUrl } from '../constants';
import { ChannelCreationDto, ChannelDto } from '../dto/ChannelDto';

export const fetchChannels = async (): Promise<ChannelDto[]> => {
  const response = await fetch(`${baseApiUrl}/channels`);
  const entities = (await response.json()) as ChannelDto[];
  return entities;
};

export const createChannel = async (channel: ChannelCreationDto): Promise<ChannelDto> => {
  const response = await fetch(`${baseApiUrl}/channels`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(channel),
  });

  const entity = (await response.json()) as ChannelDto;
  return entity;
};
