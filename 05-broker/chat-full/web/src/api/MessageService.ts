import { baseApiUrl } from '../constants';
import { MessageCreationDto, MessageDto } from '../dto/MessageDto';

export const fetchMessages = async (channelId: number): Promise<MessageDto[]> => {
  const response = await fetch(`${baseApiUrl}/channels/${channelId}/messages`);
  const entities = (await response.json()) as MessageDto[];
  return entities;
};

export const sendMessage = async (channelId: number, message: MessageCreationDto): Promise<MessageDto> => {
  const response = await fetch(`${baseApiUrl}/channels/${channelId}/messages`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });

  const entity = (await response.json()) as MessageDto;
  return entity;
};
