import React, { useContext, useEffect, useState } from 'react';
import { fetchMessages } from '../api/MessageService';
import SocketContext from '../context/SocketContext';
import UserContext from '../context/UserContext';
import { ChannelDto } from '../dto/ChannelDto';
import { MessageDto } from '../dto/MessageDto';
import { UserDto } from '../dto/UserDto';
import MessageComponent from './Message';
import SimpleInput from './SimpleInput';

export type ChannelComponentProps = {
  channel?: ChannelDto;
};

export default function ChannelComponent({ channel = null }: ChannelComponentProps) {
  const [messages, setMessages] = useState<MessageDto[]>([]);
  const user = useContext<UserDto>(UserContext);
  const socket = useContext(SocketContext);

  socket.once('message', (newMessage: MessageDto) => {
    console.log('onmessage');
    setMessages([...messages, newMessage]);
  });

  useEffect(() => {
    (async () => {
      const results = await fetchMessages(channel.id);
      setMessages(results);
    })();

    console.log(`Joining ${channel.id}`);
    socket.emit('join', channel.id);
  }, [channel.id]);

  const onMessage = (text: string) => {
    socket.emit('message', {
      userId: user.id,
      channelId: channel.id,
      text,
    });
  };

  if (!channel) {
    return <>&nbsp;</>;
  }

  return (
    <div id="messages">
      <h2>{channel.name}</h2>
      {messages.map((message) => (
        <MessageComponent key={message.id} message={message} />
      ))}

      <SimpleInput id="messageInput" placeholder="Type message and press ENTER..." onEnter={onMessage} />
    </div>
  );
}
