import React from 'react';
import { createChannel } from '../api/ChannelService';
import { ChannelDto } from '../dto/ChannelDto';
import SimpleInput from './SimpleInput';

export type ChannelsProps = {
  channels?: ChannelDto[];
  setChannels: (channels: ChannelDto[]) => unknown;
  selectedChannel?: ChannelDto;
  setSelectedChannel: (channel: ChannelDto) => unknown;
};

export default function Channels({
  channels = null,
  setChannels,
  selectedChannel = null,
  setSelectedChannel,
}: ChannelsProps) {
  if (!channels) {
    return <div>Loading...</div>;
  }

  const onCreate = async (name: string) => {
    const newChannel = await createChannel({ name });
    setChannels([...channels, newChannel]);
  };

  return (
    <div id="channels">
      <h2>Channels</h2>
      {channels.map((channel) => (
        <div
          key={channel.id}
          role="link"
          tabIndex={0}
          onKeyUp={() => setSelectedChannel(channel)}
          className={channel === selectedChannel ? 'link selected' : 'link'}
          onClick={() => setSelectedChannel(channel)}
        >
          {channel.name}
        </div>
      ))}

      <SimpleInput placeholder="New channel name..." onEnter={onCreate} />
    </div>
  );
}
