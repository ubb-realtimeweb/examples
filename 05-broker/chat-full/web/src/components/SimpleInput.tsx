import React, { useState } from 'react';

export type SimpleInputProps = {
  id?: string;
  placeholder?: string;
  onEnter: (value: string) => unknown;
};

export default function SimpleInput({
  id = null,
  placeholder = 'Enter text and press ENTER...',
  onEnter,
}: SimpleInputProps) {
  const [value, setValue] = useState('');

  const onKeyUp = (keyPressEvent: React.KeyboardEvent<HTMLInputElement>) => {
    if (keyPressEvent.keyCode === 13) {
      onEnter(value);
      setValue('');
    }
  };

  return (
    <input
      type="text"
      id={id}
      value={value}
      onChange={(e) => setValue(e.target.value)}
      onKeyUp={onKeyUp}
      placeholder={placeholder}
    />
  );
}
