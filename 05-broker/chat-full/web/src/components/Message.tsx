import React, { useContext } from 'react';
import UserContext from '../context/UserContext';
import { MessageDto } from '../dto/MessageDto';

export type MessageComponentProps = {
  message: MessageDto;
};

export default function MessageComponent(props: MessageComponentProps) {
  const { message } = props;
  const user = useContext(UserContext);

  return (
    <div className={user.id === message.user.id ? 'me' : 'them'}>
      <span className="name">{message.user.name}:&nbsp;</span>
      <span className="date">({message.time})&nbsp;</span>
      <span className="msg">{message.text}</span>
    </div>
  );
}
