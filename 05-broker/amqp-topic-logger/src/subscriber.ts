/**
 * Ez a szkript figyeli a szűrt log üzeneteket, s lekéri őket az alapértelmezett topicról
 * Parancssori argumentumokból nyerjük ki a kívánt szintet
 *
 * Meghívás:
 *   - összes üzenet:         nodemon src/subscriber.ts
 *   - csak error üzenetek:   nodemon src/subscriber.ts -l info
 */
import amqp from 'amqplib';
import crypto from 'crypto';
import yargs from 'yargs';

(async () => {
  const argv = await yargs
    .option('logLevel', {
      alias: 'l',
      describe: 'Log level of your message',
      choices: ['*', 'debug', 'info', 'warn', 'error'],
      default: '*',
    })
    .help()
    .parse();

  const clientId = crypto.randomBytes(8).toString('base64');
  const queueName = `logger-${clientId}`;
  const routingPattern = `logger.${argv.logLevel}`;

  const connection = await amqp.connect(
    {},
    {
      clientProperties: {
        connection_name: 'amqp-logger-subscriber',
      },
    },
  );
  process.once('SIGINT', () => connection.close());
  const channel = await connection.createChannel();

  // dedikált queue-t kell készítsünk a kliensünknek, mely csak neki szól,
  await channel.assertQueue(queueName, { exclusive: true });
  // s binding van rá egy topic-tól, itt a defaultot adjuk meg
  // ugyanitt adjuk meg a szűrő mintát
  await channel.bindQueue(queueName, 'amq.topic', routingPattern);

  // a dedikált queue-ra figyelünk
  channel.consume(
    queueName,
    (message) => {
      const body = JSON.parse(message?.content?.toString() || '{}');
      console.log(`${body.clientId} sent: ${body.time} ${body.logLevel} - ${body.text}`);
    },
    { noAck: true },
  );

  console.log(
    `Listening to log events on queue "${queueName}" bound to "amq.topic" with routing pattern "${routingPattern}"`,
  );
})();
