/* eslint-disable import/prefer-default-export */
export const baseApiUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/api' : '/api';
