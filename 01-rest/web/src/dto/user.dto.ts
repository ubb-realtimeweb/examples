export class UserDto {
  id: string;

  fullName: string;
}

export class UserCreateDto {
  fullName: string;
}

export class UserUpdateDto {
  fullName?: string;
}
