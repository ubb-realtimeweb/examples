export type BlogPostSearchParams = {
  term?: string;
};

export class BlogPostDto {
  id: string;

  title: string;

  date: string;

  authorId: string;
}

export class BlogPostDetailsDto extends BlogPostDto {
  content: string;
}

export class BlogPostCreateDto {
  title: string;

  content: string;

  authorId: string;
}

export class BlogPostUpdateDto {
  title?: string;

  content?: string;
}
