import { useEffect, useState } from 'react';
import { createUser, deleteUser, findAllUsers } from '../api/user.api';
import { UserDto } from '../dto';

export default function Users() {
  const [users, setUsers] = useState<UserDto[]>([]);
  const [newUsername, setNewUsername] = useState('');

  useEffect(() => {
    (async () => {
      const newUsers = await findAllUsers();
      setUsers(newUsers);
    })();
  }, []);

  const onCreate = async () => {
    const newUser = await createUser({ fullName: newUsername });
    setUsers((oldUsers) => [...oldUsers, newUser]);
  };

  const onDelete = async (userId: string) => {
    await deleteUser(userId);
    setUsers((oldUsers) => oldUsers.filter((u) => u.id !== userId));
  };

  return (
    <div>
      <div className="jumbotron">
        <div style={{ marginBottom: 10 }}>
          {users.map((user) => (
            <div key={user.id}>
              {user.fullName}
              <input type="button" className="btn btn-danger btn-sm" onClick={() => onDelete(user.id)} value="-" />
            </div>
          ))}
        </div>
        <div>
          <input
            type="text"
            className="form-control"
            name="title"
            value={newUsername}
            onChange={(event) => setNewUsername(event.target.value)}
            placeholder="New user"
          />
          <input type="button" className="btn btn-info btn-sm" onClick={onCreate} value="Create" />
        </div>
      </div>
    </div>
  );
}
