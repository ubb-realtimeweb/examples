import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import { findAllBlogPosts } from '../api/blogpost.api';
import { BlogPostDto, BlogPostSearchParams } from '../dto';
import BlogPost from './BlogPost';

export default function BlogPosts() {
  const [searchParams] = useSearchParams();
  const [blogPosts, setBlogPosts] = useState<BlogPostDto[]>([]);

  const query: BlogPostSearchParams = searchParams.get('term') ? { term: searchParams.get('term') } : {};

  useEffect(() => {
    (async () => {
      const newBlogPosts = await findAllBlogPosts(query);
      setBlogPosts(newBlogPosts);
    })();
  }, [query.term]);

  if (!blogPosts.length) {
    return <div>No posts to show</div>;
  }

  return (
    <div>
      <div className="jumbotron">
        {blogPosts.map((blogPost) => (
          <BlogPost key={blogPost.id} blogPost={blogPost} />
        ))}
      </div>
    </div>
  );
}
