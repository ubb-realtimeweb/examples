import { Db, ObjectId } from 'mongodb';
import { BlogPost } from '../model/blogpost.model';
import { Repository } from './base.repo';

/**
 * Konkrét adatelérési objektum.
 * Tartalmazhat entitás-specifikus lekérdezéseket
 */
export class BlogPostRepository extends Repository<BlogPost> {
  findByTitle(title: string): Promise<BlogPost[]> {
    const regex = new RegExp(`^.*${title}.*$`, 'i');
    return this.collection.find({ title: regex }).toArray();
  }

  findByAuthorId(authorId: ObjectId): Promise<BlogPost[]> {
    return this.collection.find({ authorId }).toArray();
  }
}

/**
 * Alapértelmezett példány
 */
export default function getBlogPostRepo(db: Db) {
  const blogPostsCollection = db.collection<BlogPost>('blogPosts');
  const blogPostRepo = new BlogPostRepository(blogPostsCollection);
  return blogPostRepo;
}
