import { IsNotEmpty, IsString, Matches } from 'class-validator';

/**
 * Kimenő DTO.
 * Nem szükséges validálni.
 */
export type UserDto = {
  id: string;
  fullName: string;
};

/**
 * Bejövő DTO-k
 * - Szükséges a validáció
 * - id-t nem tartalmaznak
 * - a részleges módosításos DTO-nál nem kötelezőek a mezők
 */

export type UserCreateDtoType = {
  fullName: string;
};

export class UserCreateDto {
  @IsString()
  @IsNotEmpty()
  @Matches(/[a-zA-Z0-9- ]+/)
  fullName: string;

  constructor(data: UserCreateDtoType) {
    this.fullName = data.fullName;
  }
}

export type UserUpdateDtoType = {
  fullName?: string;
};

export class UserUpdateDto {
  @IsString()
  @Matches(/[a-zA-Z0-9- ]+/)
  fullName?: string;

  constructor(data: UserUpdateDtoType) {
    this.fullName = data.fullName;
  }
}
