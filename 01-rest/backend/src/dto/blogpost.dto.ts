import { IsMongoId, IsNotEmpty, IsString, Length } from 'class-validator';

/**
 * Kimenő DTO.
 * Nem szükséges validálni.
 * Különbségek:
 * - 2 reprezentáció (egy rövidebb listázásnak, s egy konkrétabb
 * - különböző típusok (ObjectId, Date)
 * - _id => id
 */
export type BlogPostDto = {
  id: string;
  title: string;
  date: string;
  authorId: string;
};

export type BlogPostDetailsDto = BlogPostDto & {
  content: string;
};

/**
 * Speciális kimenő DTO
 * Amikor author szerint kérünk le blogPosts, magát az authort nem érdemes visszaírni bele
 */
export type BlogPostByAuthorDto = {
  id: string;
  title: string;
  date: string;
};

/**
 * Bejövő DTO-k
 * - Szükséges a validáció
 * - id-t nem tartalmaznak
 * - a részleges módosításos DTO-nál nem kötelezőek a mezők
 */

export type BlogPostCreateDtoType = {
  title: string;
  content: string;
  authorId: string;
};

export class BlogPostCreateDto {
  @IsString()
  @IsNotEmpty()
  @Length(0, 256)
  title: string;

  @IsString()
  @Length(0, 4096)
  content: string;

  @IsMongoId()
  @IsNotEmpty()
  authorId: string;

  constructor(data: BlogPostCreateDtoType) {
    this.title = data.title;
    this.content = data.content;
    this.authorId = data.authorId;
  }
}

export type BlogPostUpdateDtoType = {
  title?: string;
  content?: string;
};

export class BlogPostUpdateDto {
  @IsString()
  @Length(0, 256)
  title?: string;

  @IsString()
  @Length(0, 4096)
  content?: string;

  constructor(data: BlogPostUpdateDtoType) {
    this.title = data.title;
    this.content = data.content;
  }
}
