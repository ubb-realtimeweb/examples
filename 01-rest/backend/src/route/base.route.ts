// Ez az állomány egy REST API gyökerének express routerét konfigurálja.
// Betölti a további API endpoint lekezelőket.
import cors from 'cors';
import express, { Router } from 'express';
import { ObjectId } from 'mongodb';
import morgan from 'morgan';
import { BlogPostCreateDto } from '../dto/blogpost.dto';
import { UserCreateDto, UserUpdateDto } from '../dto/user.dto';
import blogPostMapper from '../mapper/blogpost.mapper';
import userMapper from '../mapper/user.mapper';
import { bodyValidator, mongoIdValidator } from '../middleware/validation';
import { BlogPost } from '../model/blogpost.model';
import { User } from '../model/user.model';
import { BlogPostRepository } from '../repo/blogpost.repo';
import { UserRepository } from '../repo/user.repo';

export default function getApiRoutes(blogPostRepo: BlogPostRepository, userRepo: UserRepository) {
  const router = Router();

  // loggoljuk csak az API kéréseket
  router.use(morgan('tiny'));

  // engedünk cross-origin kéréseket az API-ra
  router.use(cors());

  // minden testtel ellátott API hívás
  // JSON-t tartalmaz
  router.use(express.json());

  //
  // user
  //

  router.get('/users', async (req, resp) => {
    const users: User[] = await userRepo.findAll();
    resp.send(userMapper.modelsToDtos(users));
  });

  router.get('/users/:id', mongoIdValidator(), async (req, resp) => {
    const id = new ObjectId(req.params.id);
    const user = await userRepo.findById(id);
    if (!user) {
      resp.sendStatus(404);
      return;
    }
    resp.send(userMapper.modelToDto(user));
  });

  router.post('/users', bodyValidator(UserCreateDto), async (req, res) => {
    const fullname = req.body.fullName;
    if (!fullname) {
      res.status(400).send('Full name is empty!');
      return;
    }
    const newUser = userMapper.createDtoToModel(req.body);
    const user = await userRepo.create(newUser);
    res.status(201).send(userMapper.modelToDto(user));
  });

  router.patch('/users/:id', mongoIdValidator(), bodyValidator(UserUpdateDto), async (req, res) => {
    const id = new ObjectId(req.params.id);
    let user = await userRepo.findById(id);
    if (!user) {
      res.sendStatus(404);
      return;
    }
    user = userMapper.updateModelByDto(user, req.body);
    user = await userRepo.update(user);
    res.send(userMapper.modelToDto(user));
  });

  router.delete('/users/:id', mongoIdValidator(), async (req, res) => {
    const id = new ObjectId(req.params.id);
    await userRepo.delete(id);
    res.sendStatus(204);
  });

  router.get('/users/:id/blogPosts', mongoIdValidator(), async (req, res) => {
    const userId = new ObjectId(req.params.id);
    const blogPosts = await blogPostRepo.findByAuthorId(userId);
    res.status(200).send(blogPostMapper.modelsToByAuthorDtos(blogPosts));
  });

  //
  // blog post
  //

  router.get('/blogPosts', async (req, res) => {
    let blogPosts: BlogPost[];
    if (req.query.term) {
      blogPosts = await blogPostRepo.findByTitle(`${req.query.term}`);
    } else {
      blogPosts = await blogPostRepo.findAll();
    }
    const blogPostsMapped = blogPostMapper.modelsToDtos(blogPosts);
    res.send(blogPostsMapped);
  });

  router.get('/blogPosts/:id', mongoIdValidator(), async (req, resp) => {
    const id = new ObjectId(req.params.id);
    const blogPost = await blogPostRepo.findById(id);
    if (!blogPost) {
      resp.sendStatus(404);
      return;
    }
    resp.send(blogPostMapper.modelToDto(blogPost));
  });

  router.post('/blogPosts', bodyValidator(BlogPostCreateDto), async (req, res) => {
    const mappedBlog = blogPostMapper.createDtoToModel(req.body);
    const createdBlog = await blogPostRepo.create(mappedBlog);
    res.status(201).send(blogPostMapper.modelToDto(createdBlog));
  });

  router.delete('/blogPosts/:id', mongoIdValidator(), async (req, res) => {
    const id = new ObjectId(req.params.id);
    await blogPostRepo.delete(id);
    res.sendStatus(204);
  });

  router.get('/blogPosts/:id/author', mongoIdValidator(), async (req, res) => {
    const id = new ObjectId(req.params.id);
    const blogPost = await blogPostRepo.findById(id);
    if (!blogPost) {
      res.sendStatus(404);
      return;
    }
    const author = await userRepo.findById(blogPost.authorId);
    if (!author) {
      res.sendStatus(404);
      return;
    }
    res.send(userMapper.modelToDto(author));
  });

  return router;
}
