import { NextFunction, Request, Response } from 'express';

/**
 * Lehetséges hibák általánosítása
 */
export class BadRequestError extends Error {
  static statusCode = 400;

  messages: string | string[];

  constructor(messages: string | string[]) {
    super();
    this.messages = messages || 'Bad request';
  }
}

export class NotFoundError extends Error {
  static statusCode = 404;
}

/**
 * Express middleware általános hibakezelés.
 * Minden ismert errort átalakítunk megfelelő response státusszá és üzenetté.
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const handleError = (err: Error, req: Request, res: Response, next: NextFunction) => {
  err = err || new NotFoundError();

  // különböző hibatípusokból származó válasz
  if (err instanceof NotFoundError) {
    res.sendStatus(NotFoundError.statusCode);
  } else if (err instanceof BadRequestError) {
    res.status(BadRequestError.statusCode).send({ messages: err.messages });
  } else {
    res.status(500).send({ message: err.message });
  }
};
