import { UserCreateDto, UserDto, UserUpdateDto } from '../dto/user.dto';
import { Unpersisted } from '../model/base';
import { User } from '../model/user.model';

export class UserMapper {
  modelToDto(model: User): UserDto {
    return {
      id: model._id.toHexString(),
      fullName: model.fullName,
    };
  }

  modelsToDtos(models: User[]): UserDto[] {
    return models.map((model) => this.modelToDto(model));
  }

  createDtoToModel(dto: UserCreateDto): Unpersisted<User> {
    return {
      fullName: dto.fullName,
    };
  }

  updateModelByDto(model: User, dto: UserUpdateDto): User {
    return {
      _id: model._id,
      fullName: dto.fullName || model.fullName,
    };
  }
}

export default new UserMapper();
