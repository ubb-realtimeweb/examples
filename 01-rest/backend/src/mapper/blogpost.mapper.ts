import { ObjectId } from 'mongodb';
import {
  BlogPostByAuthorDto,
  BlogPostCreateDto,
  BlogPostDetailsDto,
  BlogPostDto,
  BlogPostUpdateDto,
} from '../dto/blogpost.dto';
import { Unpersisted } from '../model/base';
import { BlogPost } from '../model/blogpost.model';

export class BlogPostMapper {
  modelToDto(model: BlogPost): BlogPostDto {
    return {
      id: model._id.toHexString(),
      date: model.date.toISOString(),
      title: model.title,
      authorId: model.authorId.toHexString(),
    };
  }

  modelToDetailsDto(model: BlogPost): BlogPostDetailsDto {
    return {
      id: model._id.toHexString(),
      date: model.date.toISOString(),
      title: model.title,
      content: model.content,
      authorId: model.authorId.toHexString(),
    };
  }

  modelToByAuthorDto(model: BlogPost): BlogPostByAuthorDto {
    return {
      id: model._id.toHexString(),
      date: model.date.toISOString(),
      title: model.title,
    };
  }

  modelsToDtos(models: BlogPost[]): BlogPostDto[] {
    return models.map((model) => this.modelToDto(model));
  }

  modelsToByAuthorDtos(models: BlogPost[]): BlogPostByAuthorDto[] {
    return models.map((model) => this.modelToByAuthorDto(model));
  }

  createDtoToModel(dto: BlogPostCreateDto): Unpersisted<BlogPost> {
    return {
      title: dto.title,
      content: dto.content,
      authorId: new ObjectId(dto.authorId),
      date: new Date(),
    };
  }

  updateModelByDto(model: BlogPost, dto: BlogPostUpdateDto): BlogPost {
    return {
      _id: model._id,
      title: dto.title || model.title,
      content: dto.content || model.content,
      authorId: model.authorId,
      date: new Date(),
    };
  }
}

export default new BlogPostMapper();
