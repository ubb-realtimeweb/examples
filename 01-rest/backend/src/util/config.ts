/**
 * Globális szerverbeállítások, melyek fölülírhatóak környezeti változókból
 */

export const port = process.env.PORT || 8080;

export const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost';
export const mongoDatabaseName = process.env.MONGO_DB || 'blogposts';
export const mongoUser = process.env.MONGO_USER || 'root';
export const mongoPassword = process.env.MONGO_PASSWORD || '8UhS5R72hg';
