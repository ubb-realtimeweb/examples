import { MongoClient, MongoClientOptions } from 'mongodb';
import { mongoDatabaseName, mongoPassword, mongoUrl, mongoUser } from './config';

/**
 * Általános Mongo kapcsolat
 */

console.log(`Connecting to Mongo at ${mongoUrl}, database ${mongoDatabaseName} ...`);

// support anonymous and authenticated connection
const connectOptions: MongoClientOptions = {};
if (mongoUser && mongoPassword) {
  connectOptions.auth = { username: mongoUser, password: mongoPassword };
}

export async function getMongoDb() {
  const mongoClient = await MongoClient.connect(mongoUrl, connectOptions);
  const db = mongoClient.db(mongoDatabaseName);
  console.log('MongoDB connection successful');
  return db;
}
