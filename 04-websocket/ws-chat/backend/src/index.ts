/**
 * Chat alkalmazás ws segítségével.
 */
import WebSocket, { WebSocketServer } from 'ws';

// WS szerver
const wss = new WebSocketServer({ port: Number(process.env.PORT) || 8080 });

// websocket csatlakozásának lekezelése
wss.on('connection', (ws, req) => {
  // egyedi azonosító kibányászása a request URL-ből
  const url = new URL(`http://localhost:8080${req.url}`);
  const id = url.searchParams.get('clientId');

  console.log(`[${id}] connected from ${req.socket.remoteAddress}`);

  ws.on('message', (message) => {
    console.log(`[${id}] sent '${message}', relaying`);

    // az összes kliensnek kiküldjük az üzenetet, kivéve magunknak
    wss.clients.forEach((client) => {
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        client.send(message);
      }
    });
  });

  // socket zárásakor töröljük az aktív kliensek listájából
  ws.on('close', () => {
    console.log(`[${id}] closed connection`);
  });
});
