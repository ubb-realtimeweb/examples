import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

//
// esemény DTO
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

function ChatWebSocket() {
  const [text, setText] = useState<string>('');
  const [messages, setMessages] = useState<Array<BroadcastMessage>>([]);

  // random ID
  const clientId = useMemo(() => {
    return btoa(crypto.getRandomValues(new Uint8Array(2)).toString());
  }, []);

  // Kapcsolat megnyitása a dedikált WS felé
  const ws = useMemo(() => new WebSocket(`ws://localhost:8080/chat?clientId=${clientId}`), [clientId]);

  useEffect(() => {
    // ha üzenetet kapunk a szervertől
    ws.onmessage = async (event: MessageEvent<Blob>) => {
      console.log(event);
      const data = await event.data.text();
      const message: BroadcastMessage = JSON.parse(data);
      setMessages((m) => [...m, message]);
    };

    return () => {
      console.log('Disconnecting from WebSocket');
      ws.close();
    };
  }, [clientId]);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // üzenetet küldünk
      const message: BroadcastMessage = {
        clientId,
        text,
        time: new Date().toISOString(),
      };

      // WebSockettel küldjük az üzenetet
      ws.send(JSON.stringify(message));

      // a szöveges bemenetet ürítjük
      setMessages((m) => [...m, message]);
      setText('');
    },
    [clientId, text, messages],
  );

  return (
    <>
      <h1>Chat with WebSocket</h1>

      <div>
        <b>Status:</b> Connected as <code>{clientId}</code>
      </div>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.clientId}-${message.time}`} className={message.clientId === clientId ? 'me' : 'them'}>
            <span className="name">{message.clientId}:&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>

      {/* <!-- form, amin keresztül üzenetet küldhetünk --> */}
      <form onSubmit={sendMessage}>
        <input
          style={{ textAlign: 'right' }}
          type="text"
          value={text}
          onChange={(event) => setText(event.target.value)}
          placeholder="Type message and press ENTER..."
        />
      </form>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<ChatWebSocket />);
