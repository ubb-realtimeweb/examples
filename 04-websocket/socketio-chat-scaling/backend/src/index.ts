/**
 * Chat alkalmazás ws segítségével.
 */
import { createAdapter } from '@socket.io/redis-adapter';
import crypto from 'crypto';
import http from 'http';
import { createClient } from 'redis';
import { Server } from 'socket.io';

//
// socket io DTOs
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

interface ServerToClientEvents {
  message: (message: BroadcastMessage) => void;
  assignid: (id: string) => void;
}

interface ClientToServerEvents {
  message: (message: BroadcastMessage) => void;
}

(async () => {
  //
  // két Redis kapcsolat (egy küldésre, egy fogadásra)
  //
  const pubClient = createClient({ url: 'redis://localhost:6379' });
  const subClient = pubClient.duplicate();

  await Promise.all([pubClient.connect(), subClient.connect()]);
  const server = http.createServer();

  // Socket.IO szerver
  const io = new Server<ClientToServerEvents, ServerToClientEvents>(server, {
    cors: { origin: '*' },
    //
    // adapter hozzáadása a socket.io szerverhez
    //
    adapter: createAdapter(pubClient, subClient),
  });

  // websocket csatlakozásának lekezelése
  io.on('connection', (socket) => {
    // egyedi azonosító random generálása
    const id = crypto.randomBytes(8).toString('base64');
    console.log(`[${id}] Socket connected: ${socket.conn.remoteAddress}`);

    socket.on('message', (message) => {
      console.log(`[${id}] sent '${message.text}', relaying`);

      // az alábbi minden MÁS kliensnek küldi az üzenetet (relay)
      socket.broadcast.emit('message', message);
      // az alábbi mindenkinek küldené az üzenetet (a jelen kliensnek is)
      // io.emit('message', message);
    });

    // egyedi azonosító eseményben való küldése
    socket.emit('assignid', id);
  });

  const port = process.env.PORT || 8080;
  server.listen(port, () => {
    console.log(`Server listening on http://localhost:${port}/ ...`);
  });
})();
