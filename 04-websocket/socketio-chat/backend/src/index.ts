/**
 * Chat alkalmazás ws segítségével.
 */
import crypto from 'crypto';
import http from 'http';
import { Server } from 'socket.io';

//
// socket io DTOs
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

interface ServerToClientEvents {
  message: (message: BroadcastMessage) => void;
  assignid: (id: string) => void;
}

interface ClientToServerEvents {
  message: (message: BroadcastMessage) => void;
}

const server = http.createServer();

// Socket.IO szerver
const io = new Server<ClientToServerEvents, ServerToClientEvents>(server, {
  cors: { origin: '*' },
});

// websocket csatlakozásának lekezelése
io.on('connection', (socket) => {
  // egyedi azonosító random generálása
  const id = crypto.randomBytes(8).toString('base64');
  console.log(`[${id}] Socket connected: ${socket.conn.remoteAddress}`);

  socket.on('message', (message) => {
    console.log(`[${id}] sent '${message.text}', relaying`);

    // az alábbi minden MÁS kliensnek küldi az üzenetet (relay)
    socket.broadcast.emit('message', message);
    // az alábbi mindenkinek küldené az üzenetet (a jelen kliensnek is)
    // io.emit('message', message);
  });

  // egyedi azonosító eseményben való küldése
  socket.emit('assignid', id);
});

const port = process.env.PORT || 8080;
server.listen(port, () => {
  console.log(`Server listening on http://localhost:${port}/ ...`);
});
